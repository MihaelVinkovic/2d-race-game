﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RaceManager : MonoBehaviour {

    private SpriteRenderer C1,C2,C3;
    private TransitionHelper carData;
    private RaceCar RaceCar1, RaceCar2, RaceCar3;


    [HideInInspector]
    public bool Win;
    [HideInInspector]
    public int whoWon = 99;


    public List<Sprite> carColors = new List<Sprite>();
    public GameObject winnerPicture, backgound, Main;
    public GameObject btn1, btn2;




	// Use this for initialization
	void Start () {
        C1 = GameObject.Find("Car1").GetComponent<SpriteRenderer>();
        C2 = GameObject.Find("Car2").GetComponent<SpriteRenderer>();
        C3 = GameObject.Find("Car3").GetComponent<SpriteRenderer>();

        carData = FindObjectOfType<TransitionHelper>();
        C1.sprite = carColors[(int)carData.c1.color];
        C2.sprite = carColors[(int)carData.c2.color];
        C3.sprite = carColors[(int)carData.c3.color];


        winnerPicture.SetActive(false);
        backgound.SetActive(false);
        Main.SetActive(false);
        btn1.SetActive(false);
        btn2.SetActive(false);

        StartRace();
        
    }

    // Update is called once per frame
    void Update () {
        if (Win)
        {
            winnerPicture.SetActive(true);
            backgound.SetActive(true);
            Main.SetActive(true);
            winnerPicture.GetComponent<Image>().sprite = carColors[whoWon];
            btn1.SetActive(true);
            btn2.SetActive(true);

            //winnerPicture.sprite = carColors[whoWon];
        }
	}


    void StartRace()
    {
        RaceCar1 = GameObject.Find("Car1").GetComponent<RaceCar>();
        RaceCar2 = GameObject.Find("Car2").GetComponent<RaceCar>();
        RaceCar3 = GameObject.Find("Car3").GetComponent<RaceCar>();


        RaceCar1.SetData(carData.c1.engine, carData.c1.tires, carData.c1.height);
        RaceCar2.SetData(carData.c2.engine, carData.c2.tires, carData.c2.height);
        RaceCar3.SetData(carData.c3.engine, carData.c3.tires, carData.c3.height);
    }



    public void RetryLevel()
    {
        winnerPicture.SetActive(false);
        backgound.SetActive(false);
        Main.SetActive(false);

        Win = false;


        GameObject ResetCar1 = GameObject.Find("Car1");
        GameObject ResetCar2 = GameObject.Find("Car2");
        GameObject ResetCar3 = GameObject.Find("Car3");

        ResetCar1.transform.position = new Vector3(-5, -2.69f, 0);
        ResetCar1.GetComponent<Rigidbody2D>().velocity = Vector2.zero;

        ResetCar2.transform.position = new Vector3(0, -2.69f, 0);
        ResetCar2.GetComponent<Rigidbody2D>().velocity = Vector2.zero;

        ResetCar3.transform.position = new Vector3(5, -2.69f, 0);
        ResetCar3.GetComponent<Rigidbody2D>().velocity = Vector2.zero;

        btn1.SetActive(false);
        btn2.SetActive(false);



    }
}
