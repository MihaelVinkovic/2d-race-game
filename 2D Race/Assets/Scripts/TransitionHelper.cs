﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TransitionHelper : MonoBehaviour {

    private CarSelector CS;
    public Car c1 = new Car();
    public Car c2 = new Car();
    public Car c3 = new Car();

    private void Awake()
    {
        if(SceneManager.GetActiveScene().buildIndex == 1)
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    // Use this for initialization
    void Start () {
        CS = FindObjectOfType<CarSelector>();
        
	}
	
	// Update is called once per frame
	void Update () {
        //Debug.Log(c1.color);
        //Debug.Log(c2.engine);
    }


    public void SaveValues()
    {
        c1.color = CS.C1Color.value;
        c1.tires = CS.C1Tires.value;
        c1.engine = CS.C1Engine.value;
        c1.height = CS.C1Body.value;


        c2.color = CS.C2Color.value;
        c2.tires = CS.C2Tires.value;
        c2.engine = CS.C2Engine.value;
        c2.height = CS.C2Body.value;


        c3.color = CS.C3Color.value;
        c3.tires = CS.C3Tires.value;
        c3.engine = CS.C3Engine.value;
        c3.height = CS.C3Body.value;

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

    }
}




public class Car{
    public float color = 0;
    public float tires = 0;
    public float engine = 0;
    public float height = 0;
}

