﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicManager : MonoBehaviour {

    public AudioClip[] Sounds;
    public AudioSource audioSource;



    private void Awake()
    {
        DontDestroyOnLoad(gameObject);

        if (SceneManager.GetActiveScene().buildIndex == 0)
        {
            audioSource.volume = 1;
            AudioClip play = Sounds[0];
            audioSource.clip = play;
            audioSource.loop = false;
            audioSource.Play();
        }
    }







}
