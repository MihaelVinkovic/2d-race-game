﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CarSelector : MonoBehaviour {


    public List<Sprite> Colors = new List<Sprite>();
    int C1pick = 0, C2pick = 0, C3pick = 0;

    private Image Car1Image, Car2Image, Car3Image;

    [HideInInspector]
    public Slider C1Color, C1Tires, C1Engine, C1Body;
    [HideInInspector]
    public Slider C2Color, C2Tires, C2Engine, C2Body;
    [HideInInspector]
    public Slider C3Color, C3Tires, C3Engine, C3Body;




    

	// Use this for initialization
	void Start () {
        Car1Image = GameObject.Find("Car1Image").GetComponent<Image>();
        Car1Image.sprite = Colors[C1pick];
        Car2Image = GameObject.Find("Car2Image").GetComponent<Image>();
        Car2Image.sprite = Colors[C2pick];
        Car3Image = GameObject.Find("Car3Image").GetComponent<Image>();
        Car3Image.sprite = Colors[C3pick];

        C1Color = GameObject.Find("C1ColorSlider").GetComponent<Slider>();
        C1Tires = GameObject.Find("C1TiresSlider").GetComponent<Slider>();
        C1Engine = GameObject.Find("C1EngineSlider").GetComponent<Slider>();
        C1Body = GameObject.Find("C1BodySlider").GetComponent<Slider>();

        C2Color = GameObject.Find("C2ColorSlider").GetComponent<Slider>();
        C2Tires = GameObject.Find("C2TiresSlider").GetComponent<Slider>();
        C2Engine = GameObject.Find("C2EngineSlider").GetComponent<Slider>();
        C2Body = GameObject.Find("C2BodySlider").GetComponent<Slider>();

        C3Color = GameObject.Find("C3ColorSlider").GetComponent<Slider>();
        C3Tires = GameObject.Find("C3TiresSlider").GetComponent<Slider>();
        C3Engine = GameObject.Find("C3EngineSlider").GetComponent<Slider>();
        C3Body = GameObject.Find("C3BodySlider").GetComponent<Slider>();
    }


#region    ** C1 CAR SELECTIONS **

    public void C1ChangeColorRight()
    {
        if(C1pick <= 2)
        {
            C1Color.value += 1;
            C1pick += 1;
            Car1Image.sprite = Colors[C1pick];
        }
    }

    public void C1ChangeColorLeft()
    {
        if (C1pick >= 1)
        {
            C1Color.value -= 1;
            C1pick -= 1;
            Car1Image.sprite = Colors[C1pick];
        }
    }



    public void C1TiresUp()
    {
        if(C1Tires.value <= 2)
        {
            C1Tires.value += 1;
        } 
    }


    public void C1TiresDown()
    {
        if(C1Tires.value >= 1)
        {
            C1Tires.value -= 1;
        }
    }


    public void C1EngineUp()
    {
        if (C1Engine.value <= 2)
        {
            C1Engine.value += 1;
        }
    }


    public void C1EngineDown()
    {
        if (C1Engine.value >= 1)
        {
            C1Engine.value -= 1;
        }
    }


    public void C1BodyUp()
    {
        if (C1Body.value <= 2)
        {
            C1Body.value += 1;
        }
    }


    public void C1BodyDown()
    {
        if (C1Body.value >= 1)
        {
            C1Body.value -= 1;
        }
    }

    #endregion


#region ** C2 CAR SELECTIONS ** 
    public void C2ChangeColorRight()
    {
        if (C2Color.value <=2)
        {
            C2Color.value += 1;            
            Car2Image.sprite = Colors[(int)C2Color.value];
        }
    }

    public void C2ChangeColorLeft()
    {
        if (C2Color.value >= 1)
        {
            C2Color.value -= 1;            
            Car2Image.sprite = Colors[(int)C2Color.value];
        }
    }


    public void C2TiresUp()
    {
        if (C2Tires.value <= 2)
        {
            C2Tires.value += 1;
        }
    }


    public void C2TiresDown()
    {
        if (C2Tires.value >= 1)
        {
            C2Tires.value -= 1;
        }
    }


    public void C2EngineUp()
    {
        if (C2Engine.value <= 2)
        {
            C2Engine.value += 1;
        }
    }


    public void C2EngineDown()
    {
        if (C2Engine.value >= 1)
        {
            C2Engine.value -= 1;
        }
    }


    public void C2BodyUp()
    {
        if (C2Body.value <= 2)
        {
            C2Body.value += 1;
        }
    }


    public void C2BodyDown()
    {
        if (C2Body.value >= 1)
        {
            C2Body.value -= 1;
        }
    }



    #endregion


#region ** C3 CAR SELECTIONS **

    public void C3ChangeColorRight()
    {
        if (C3Color.value <= 2)
        {
            C3Color.value += 1;
            Car3Image.sprite = Colors[(int)C3Color.value];
        }
    }

    public void C3ChangeColorLeft()
    {
        if (C3Color.value >= 1)
        {
            C3Color.value -= 1;
            Car3Image.sprite = Colors[(int)C3Color.value];
        }
    }


    public void C3TiresUp()
    {
        if (C3Tires.value <= 2)
        {
            C3Tires.value += 1;
        }
    }


    public void C3TiresDown()
    {
        if (C3Tires.value >= 1)
        {
            C3Tires.value -= 1;
        }
    }


    public void C3EngineUp()
    {
        if (C3Engine.value <= 2)
        {
            C3Engine.value += 1;
        }
    }


    public void C3EngineDown()
    {
        if (C3Engine.value >= 1)
        {
            C3Engine.value -= 1;
        }
    }


    public void C3BodyUp()
    {
        if (C3Body.value <= 2)
        {
            C3Body.value += 1;
        }
    }


    public void C3BodyDown()
    {
        if (C3Body.value >= 1)
        {
            C3Body.value -= 1;
        }
    }


    #endregion



}
