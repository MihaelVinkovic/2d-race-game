﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaceCar : MonoBehaviour {


    private Rigidbody2D rigid;
    private RaceManager rM;
    private TransitionHelper tH;

    private float speed;



	// Use this for initialization
	void Start () {
        rigid = GetComponent<Rigidbody2D>();
        rM = FindObjectOfType<RaceManager>();
        tH = FindObjectOfType<TransitionHelper>();
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        //transform.position += new Vector3(0, speed, 0) * Time.deltaTime;
        rigid.AddForce(new Vector2(1, speed));
        if(transform.position.y >= 317.62f)
        {
            rM.Win = true;
            if(gameObject.name == "Car1")
            {
                rM.whoWon = (int)tH.c1.color;
            } else if (gameObject.name == "Car2")
            {
                rM.whoWon = (int)tH.c2.color; ;
            }else if (gameObject.name == "Car3")
            {
                rM.whoWon = (int)tH.c3.color; ;
            }
        }
	}

    public void SetData(float engine, float tires, float body)
    {
        float a = 0, b = 0, c = 0;
        if(engine == 0)
        {
            a = 1;
        }else if(engine == 1)
        {
            a = 2;
        }else if(engine == 2)
        {
            a = 3;
        }else if(engine >= 2)
        {
            a = 4;
        }



        if (tires == 0)
        {
            b = 1;
        }
        else if (tires == 1)
        {
            b = 2;
        }
        else if (tires == 2)
        {
            b = 3;
        }
        else if (tires >= 2)
        {
            b = 4;
        }


        if (body == 0)
        {
            c = 1;
        }
        else if (body == 1)
        {
            c = 2;
        }
        else if (body == 2)
        {
            c = 3;
        }
        else if (body >= 2)
        {
            c = 4;
        }

        speed = (a + b + c ) / 5;
    }


}
