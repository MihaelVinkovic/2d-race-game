﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowFastestPlayer : MonoBehaviour {

    public GameObject c1, c2, c3;
    private RaceManager rM;


	// Use this for initialization
	void Start () {
        rM = FindObjectOfType<RaceManager>();
	}
	
	// Update is called once per frame
	void Update () {
        if (!rM.Win)
        {
            if (c1.transform.position.y > c2.transform.position.y)
            {
                if (c1.transform.position.y > c3.transform.position.y)
                {
                    transform.position = new Vector3(0, c1.transform.position.y, -10);
                }
                else
                {
                    transform.position = new Vector3(0, c3.transform.position.y, 0);
                }

            }
            else
            {
                transform.position = new Vector3(0, c2.transform.position.y, -10);
            }
        }
	}
}
